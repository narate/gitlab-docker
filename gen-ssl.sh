#!/bin/bash
echo "Generate SSL for GitLab"
mkdir -p ssl
echo "+ ssl/gitlab.local.key"
openssl genrsa -out ssl/gitlab.local.key 2048

echo "+ ssl/gitlab.local.csr"
openssl req -new -key ssl/gitlab.local.key -out ssl/gitlab.local.csr

echo "+ ssl/gitlab.local.crt"
openssl x509 -req -days 3650 -in ssl/gitlab.local.csr -signkey ssl/gitlab.local.key  -out ssl/gitlab.local.crt

echo "Done"

